For the E2E flow automation execution details, please refer the dashboard for results, screenshots and the video.

**https://dashboard.cypress.io/#/projects/fn49y8/runs/20/specs**


The Code can be optimized to have the below features:
    * object locator at one place for a easy maintenance
    * Create reusable functions(eg: size selector, color selector), Filtering, etc..
    * Need to have many API level validations

**Sample Report:****
  Running: e2e-tests.spec.js...                                                            (1 of 1) 
  Estimated: 56 seconds


  **ABOUT YOU E2E Tests**
1.     ✓ HomePage => Check the cart => Then Search product category (27691ms)
1.     ✓ Apply Filters for the category (3062ms)
1.     ✓ Choose the product and browse through various details (8170ms)
1.     ✓ Select the size and the colour (2167ms)
1.     ✓ Add to cart and Check the cart value and the Item quantity (1648ms)
1.     ✓ Checkout (1026ms)
1.     ✓ Sign-up New user (38726ms)
1.     ✓ Enter shipping address (5236ms)
1.     ✓ Choose Payment Option (6186ms)
1.     ✓ Apply Coupon  (188ms)
1.     ✓ Verify for the Final price and proceed (571ms)
1.     ✓ Wait for the Financial Details Page and provide credentials (177ms)
1.     ✓ Wait for the Financial Data Page (106ms)
1.     ✓ Wait for the Bank Selection Page (94ms)
1.     ✓ Wait for the Konto Page (263ms)
1.     ✓ Wait for Final Confiramation (64ms)
1.     ✓ Wait for Confirm (67ms)





  **17 passing (2m)**

  
  **(Results)**

*   ┌────────────────────────────────────┐
*   │ Tests:        17                   │
*   │ Passing:      17                   │
*   │ Failing:      0                    │
*   │ Pending:      0                    │
*   │ Skipped:      0                    │
*   │ Screenshots:  0                    │
*   │ Video:        true                 │
*   │ Duration:     1 minute, 39 seconds │
*   │ Estimated:    56 seconds           │
*   │ Spec Ran:     e2e-tests.spec.js    │
*   └────────────────────────────────────┘

  
  **(Video)**

  * Started processing:   Compressing to 32 CRF
  * Compression progress:  53%
  * Finished processing:  /Users/gajendranc/pgit/about-you-cypress-tests/cypress/videos/e2e-tests.spec.js.mp4 (19 seconds)


  **(Uploading Results)**

  * Done Uploading (1/1) /Users/gajendranc/pgit/about-you-cypress-tests/cypress/videos/e2e-tests.spec.js.mp4