const users = {
  default: {
    firstname: 'Gajendran',
    lastname: 'Chandrappa',
    email: 'gajendran.testing@',
    gender: 'male',
    password: 'Test$',
    productToSearch: 'Shirt',
    addressStreet: 'Münterweg 00001',
    addressLandmark: 'Somthing',
    addressPostal: '22114',
    addressCity: 'Hamburg',
    addressCountry: 'Deutchland',
    dob: '12.03.1982'


  }
};

export default users;
